"""
作者：Jason Xie
邮箱：xjxfly@qq.com
QQ交流群：1001030977

说明：
这是基础包，是一些通用功能（纯函数）的集合。
包内提供了一些接口，具体有哪些接口可用及怎样调用，请用 help(包名)  查看，
如果导入包时有 as 过的话，则用 help(as过后的别名) 查看。

"""
# -----------------------------
# 本文件 __init__.py 会在其所在的 package 被 import 时得到执行，这点要知道。 install 时是不会执行 __init__.py 的，但会执行 setup.py 中的代码。总之：
# python setup.py install 是执行 setup.py ，不执行 __init__.py 
# import package 是执行 __init__.py ， 不执行 setup.py 

# 定义可以暴露给外部的接口，必须放在 __all__ 中，且外部只有用 from import 引用时才有效，直接 import 的话，这里的 __all__ 不起作用。；
# 另外 __all__ 也是给 help() 和 pydoc 看的，python 内置函数 help() 调用包或模块时，如果模块或包内有 __all__，则只返回 __all__ 中列出的接口的使用方法；否则返回全部接口的使用方法；
# pydoc 遇 __all__ 时也将只对 __all__ 中描述的函数开头注释导出 
# 所以 __all__ 既是给 from import 看的，也是给内置函数 help() 看的，也是给 pydoc 看的
# 另外 PEP8 建议把 __all__ 放在模块注释后，放在除 from future 之外的其他 import 前


__version__ = '3.1'
__author__ = 'Jason Xie'

# 由于在包内，import 同级目录下的 py 文件必须以 . 开头！！（另外要注意：若不在包内， import 同级目录下的 py 文件必须不能以 . 开头！！
from .jxbase_lib import *

try:
	__all__ = get_api_list()
except:
	pass

